#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include <curses.h>

struct game {
	int cur_x;
	int cur_y;
	int *given;
	int *fixed;
};

void print_frame() {
	mvaddstr( 0, 0, "+-------+-------+-------+");
	mvaddstr( 1, 0, "|       |       |       |");
	mvaddstr( 2, 0, "|       |       |       |");
	mvaddstr( 3, 0, "|       |       |       |");
	mvaddstr( 4, 0, "+-------+-------+-------+");
	mvaddstr( 5, 0, "|       |       |       |");
	mvaddstr( 6, 0, "|       |       |       |");
	mvaddstr( 7, 0, "|       |       |       |");
	mvaddstr( 8, 0, "+-------+-------+-------+");
	mvaddstr( 9, 0, "|       |       |       |");
	mvaddstr(10, 0, "|       |       |       |");
	mvaddstr(11, 0, "|       |       |       |");
	mvaddstr(12, 0, "+-------+-------+-------+");
}

struct game init_game() {
	struct game g;
	g.cur_x = 0;
	g.cur_y = 0;
	g.given = (int *) malloc(sizeof(int) * 81);
	if (!g.given)
		err(1, NULL);
	g.fixed = (int *) malloc(sizeof(int) * 81);
	if (!g.fixed)
		err(1, NULL);
	return g;
}

void move_cursor(struct game g) {
	/* TODO convert in-game coordinates to real x,y */
	move(g.cur_x, g.cur_y);
}

int main() {
	struct game g;
	int c, playing = 1;
	if (pledge("stdio rpath tty", NULL) == -1)
		err(1, "pledge");
	initscr();
	noecho();
	g = init_game();
	print_frame();
	move_cursor(g);
	refresh();
	while (playing) {
		switch (c = getch()) {
			case 'q':
				playing = 0;
				break;
		}
	}
	endwin();
	return 0;
}
