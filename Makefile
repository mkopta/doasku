CFLAGS=-g -O0 -Wall -Wextra -pedantic -ansi
CLIBS=-lcurses

doasku: doasku.c
	cc $(CFLAGS) $(CLIBS) -o doasku doasku.c

bs:
	cc -O2 -pipe  -Werror-implicit-function-declaration -MD -MP  -c bs.c
	cc   -o bs bs.o -lcurses

clean:
	rm -f bs.d bs.o bs doasku
